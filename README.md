## Specification
Inputs:
  - Open app, indicate you need a bus

Outputs:
  - ETA/Location

## References
- [Ref.1](https://medium.com/@mckinnon/draft-f1d197c4540e)
- [Ref.2](https://docs.google.com/document/d/1TtclkHz41aue17P2BRaCvayyI541PUkH4sIoM_wnLO8/edit?usp=sharing)

## License
Public Domain
